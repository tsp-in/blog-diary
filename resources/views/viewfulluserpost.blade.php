@extends('layouts.app')

@section('title') User's Diary @stop

@section('content')
<div class="container">
	@foreach($errors->all() as $error)
	@if($error != "msg")
		<ul class="alert alert-success alert-dismissable col-md-6 col-md-offset-1">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
			<li> {{ $error }} </li>
		</ul>
	@endif
	@endforeach
<div class="page-heading">{{ $diary[0]->getback->username }}'s Diary</div>

	<table class="table table-hover table-bordered" style="font-family: 'Slabo 27px', serif;">
      <thead>
	    <tr> 
			<th>User Id</th>
			<th>Date</th>
			<th>Time</th>
			<th>Task</th>
            <th>Action</th>  
		</tr>
		</thead>
	@foreach($diary as $d)
	<tbody>
        <tr>
	   		<td>{{ $d->writer_id }}</td>
       		<td>{{ $d->date }}</td>
	   		<td>{!! $d->updated_at->diffForHumans() !!}</td>
	   		<td>{!! $d->task !!}</td>
            <td>
				<span><a href="../read/{{$d->id}}">Read </a></span>
				<span> <a href="../edit/{{$d->id}}">Edit </a></span>
				<span id="click" value="{{$d->id}}" onclick="deletefulluserpost({{ $d->id }})"><a href="#" >Delete</a></span>
	  	    </td>
	    </tr>
       		@endforeach
     </tbody>
</table>
@stop