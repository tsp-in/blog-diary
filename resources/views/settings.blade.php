@extends('layouts.app')

@section('content')

@if($errors->any())
	@if(($errors->first() != "msg"))
	<ul class="alert alert-danger alert-dismissable col-md-6 col-md-offset-1">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<li> {{ $errors->first() }} </li>
	</ul>
	@else
	<ul class="alert alert-info alert-dismissable col-md-6 col-md-offset-1">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		@foreach($errors->all() as $error)
			@if($error != "msg")
			<li> {{ $error }} </li>
			@endif
		@endforeach
	</ul>
	@endif
@endif


<br /><br />

<div class="container-fluid">
<div class="row center-block">
<form id="details" name="details" method="GET" action="" >
<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
	{{ csrf_field() }}
	  <li><a href="#" id="showusernamechangeform">Change Name</a></li>
	  <li><a href="#" id="showpwdchangeform">Change Password</a></li>
    </ul>
  
</nav>	
</form>
</div>
</div>

<div class="container">

{!! Form::open(array('url' => '/changename','id' => 'changename', 'class' => 'form-horizontal col-md-offset-1', 'name' => 'changename')) !!}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">New name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $name }}" required="required" pattern="[A-Za-z0-9 ]+{3,60}" title="Min.three and max.60 alphanumeric characters are only allowed">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="submit" class="btn btn-primary" value="Change name">
                                </input>
                            </div>
                        </div>
{!! Form::close() !!}
</div>

<div class="container">
{!! Form::open(array('url' => '/changepassword','id' => 'changepwd', 'class' => 'form-horizontal col-md-offset-1', 'name' => 'changepwd','onsubmit' => 'return confirmed(this)')) !!}

                    <label for="oldpwd" class="col-md-4 control-label">Old Password</label>
                            <div class="col-md-6">
                                <input id="oldpwd" type="password" class="form-control" name="oldpwd" value="{{ old('oldpwd') }}" required="required" pattern="[A-Za-z0-9]{3,60}" title="Min.three and max.60 alphanumeric characters are only allowed">	
							</div> 
							<br><br>
					<label for="oldpwd" class="col-md-4 control-label">New Password</label>
                            <div class="col-md-6">
                                <input id="pwd" type="password" class="form-control" name="pwd" value="{{ old('pwd') }}" required="required" pattern="[A-Za-z0-9]{3,60}" title="Min.three and max.60 alphanumeric characters are only allowed">
							</div>
							<br><br>
					<label for="oldpwd" class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
                                <input id="pwd_confirmation" type="password" class="form-control" name="pwd_confirmation" value="{{ old('pwd_confirmation') }}" required="required" pattern="[A-Za-z0-9]{3,60}" title="Min.three and max.60 alphanumeric characters are only allowed">
							</div>
							<br><br>
						<div id="password_notmatched" style="margin-left:35%">
						</div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="submit" class="btn btn-primary" value="Change Password">
                                </input>
                            </div>
                        </div>
</form>
</div>

@stop