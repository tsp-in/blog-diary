@extends('layouts.app')

@section('title') View Pages @stop

@section('content')
	@foreach($errors->all() as $error)
	@if($error != "msg" && $error != "errors")
		<ul class="alert alert-info alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
			<li> {{ $error }} </li>
		</ul>
	@endif
	@endforeach
	<div class="page-heading">Pages From Your Diary</div>
	<table class="table table-hover table-bordered" style="font-family: 'Slabo 27px', serif;" id="viewtable">
      	<thead>
			<tr> 
				<th>Diary.No</th>
				<th>Name</th>
				<th>Date</th>
				<th>Last Updated Time</th>
				<th>Task</th>
				<th>Actions</th>  
			</tr>
		</thead>
		<tbody>
		@foreach($diary as $d)
			<tr>
				<td>{{ $d->id }}</td>
				<td>{{ $d->getBack->username }}</td>
				<td>{{ $d->date }}</td>
				<td>{!! $d->updated_at->diffForHumans() !!}</td>
				<td>{!! $d->task !!}</td>
				<td>
					<span><a href="read/{{$d->id}}">Read</a> </span>
					<span> <a href="edit/{{$d->id}}">Edit</a> </span>
					<span value="{{$d->id}}" onclick="deletepost({{ $d->id }})"><a href="#" >Delete</a></span>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
<!-- 
<div class="container-fluid" style="margin-right:10%;position:relative; text-align:right;">
		<span class="dropdown">
			<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" id="test">Sort Based on
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu">
			 <li><a href="#" id="sortdatetime">Date & Time</a></li>
			 <li><a href="#" id="sorttask">Task</a></li>
			</ul>
		</span>
 </div> -->
 @stop

 @section('scripts')
 <script>
 	$(function() {
		 $('.nav .view').addClass('active');
	 });
 </script>
 @stop