@extends('layouts.app')

@section('title') Read Page @stop

@section('content')
	<div class="page-heading">{{ $read->updated_at->diffForHumans() }}, {!! $read->getBack->username !!} has written for {{ $read->date }}:</div>
	<div class="diary-container col-md-6 col-md-offset-3">
		<div class="panel panel-default" >
			<div class="panel-heading">What You have done?</div>
			<div class="panel-body">{!! $read->task !!}</div>
		</div>
		<div class="panel panel-default" >
			<div class="panel-heading">What You have learned?</div>
			<div class="panel-body">{!! $read->learning !!}</div>
		</div>
		<div class="panel panel-default" >
			<div class="panel-heading">What problems did you faced?</div>
			<div class="panel-body">{!! $read->problems !!}</div>
		</div>
 		<center><a type="button" class="btn btn-default" href="#" id="press">Back</a></center>
	</div>
 @stop