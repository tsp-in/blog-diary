@extends('layouts.app')

@section('title') Edit Page @stop

@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
@stop

@section('content')
<div class="page-heading">Edit Your Page</div>
@if ($errors->any())
		@foreach ($errors->all() as $error)
			@if($error != "errors" && $error != "msg")					
			<ul class="alert alert-info alert-dismissable col-md-6 col-md-offset-1">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
					<li />{{ $error }}
			</ul>
			@endif
		@endforeach
@endif

{!! Form::Open(array('url'=>'/edit','method'=>'POST','id' => 'newblog','class' => 'form-horizontal'))  !!}
		<input type="hidden" value="{{ $edit->id }}" name="editcheck" id="editcheck">
	<div class="form-group col-sm-7 col-md-12">	
		<label id="Date">Date : </label>
		<input type="text" id="date" name="date" class="form-control" placeholder="Pick date" value="{{ $edit->date }}"  />
	</div>
	
	<div class="form-group col-sm-7 col-md-12">
		<label id="task">What you have done today?</label>
		<textarea cols="15" rows="10" name="task" id="task" placeholder="What you have done today?" class="form-control summernote" autocomplete="off">{{ $edit->task}}</textarea>
	</div>

	<div class="form-group col-sm-7 col-md-12">
		<label id="learning">What things you learned?</label>
		<textarea cols="15" rows="10" name="learning" id="learning" placeholder="What things you learned?" class="form-control summernote" autocomplete="off" >{{ $edit->learning }}</textarea>
	</div>

	<div class="form-group col-sm-7 col-md-12">
		<label id="problems">What problems did you faced?</label>
		<textarea cols="15" rows="10" name="problems" id="problems" placeholder="What problems did you faced?" class="form-control summernote" autocomplete="off" >{{ $edit->problems }}</textarea>
	</div>
      
	<div class="form-group col-sm-12 col-md-12" style="text-align:center;">
    	<input type="submit" class="btn btn-primary btn-block" value="Submit"></input>
    </div>
	  

{!! Form::close()  !!}
@stop

@section('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{asset('newblog.js')}}"></script>	
<script>
$(function() {
  	$( "#date" ).datepicker('option', {maxDate: new Date()});	
  	$('.summernote').summernote({height: 200});
});
</script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
@stop