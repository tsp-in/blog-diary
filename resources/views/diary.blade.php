@extends('layouts.app')

@section('title') Memeber Login @stop

@section('content')
	@if (Session::has('errors'))
		@if(count($errors) == 1 && $errors->first() == "msg")
			<ul class="alert alert-success alert-dismissable">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<li>Successfully created your account</li>
			</ul>
		@endif
	@endif
		
	@if (Session::has('errors') && !($errors->first() == "msg"))		
		<ul class="alert alert-danger alert-dismissable" align="left">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			@foreach ($errors->all() as $error)	
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	@endif
	<div class="col-lg-6" style="text-align:center;margin-left: 25%;margin-top:1%;">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="glyphicon glyphicon-user"></i> Member Login</div>
			<div class="panel-body">
				{!! Form::open(array('url' => '/signin','id' => 'signInForm')) !!}
				<div class="form-group col-lg-6 col-lg-offset-3">
					<label>Username</label>
					<input type="text" name="username" id="username" placeholder="Username"  class="form-control" autocomplete="off" required="required" pattern="[A-Za-z0-9]{3,20}" title="Min.three and max.20 alphanumeric characters are only allowed"></input>
				</div>
				<div class="form-group col-lg-6 col-lg-offset-3">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" placeholder="Password" class="form-control" autocomplete="off" required="required" pattern="[A-Za-z0-9]{3,20}" title="Min.three and max.20 alphanumeric characters are only allowed"></input>
				</div>
				<div class="form-group col-lg-6 col-lg-offset-3">
					<input type="submit" id="signInSubmit" class="btn btn-primary btn-block" value="Login"></input>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@stop