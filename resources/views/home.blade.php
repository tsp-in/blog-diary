@extends('layouts.app')

@section('title') Home @stop

@section('content')
@if ($errors->any())
		@foreach ($errors->all() as $error)
			@if($error != "msg")
			<ul class="alert alert-info alert-dismissable col-md-6 col-md-offset-1">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
					<li />{{ $error }}
			</ul>
			@endif
		@endforeach
@endif
	<br/>

	<div class="container-fluid">
    	<div class="diary" style="float:right; margin-bottom:1px;">
    	<a class="btn btn-success glyphicon glyphicon-plus-sign" id="set"  href="{{ url('/set') }}"> New Page</a>
		</div>
 
        <table class="table table-hover table-bordered" style="font-family: 'Slabo 27px', serif;">
      		<thead>
	    		<tr>
					<th style="width:10%;">User.ID</th>  
					<th style="width:10%;">Date</th>
					<th>Task</th>  
				</tr>
            </thead>
			<tbody class="viewbody">
            @foreach($diary as $d)
				<tr onclick="readpost({{ $d->id }})" style="cursor:pointer;">
				<td><span value="{{ $d->id }}">{{$d->id }}</span> </td>
       				<td>{{ $d->date }}</td>
	   				<td>{!! $d->task !!}</td>
	    		</tr>
       		@endforeach
     		</tbody>
	   </table>
    </div>

@stop