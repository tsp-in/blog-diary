@extends('layouts.app')

@section('title') List Users @stop

@section('content')
@if (Session::has('errors'))	
	@if( $errors->first()  == "message")
		<ul class="alert alert-success alert-dismissable col-md-2 col-md-offset-1">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
			<li> Admin added </li>
		</ul>
	@else
	@foreach($errors->all() as $error)
	@if($error != "msg")
		<ul class="alert alert-info alert-dismissable col-md-6 col-md-offset-1">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
			<li> {{ $error }} </li>
		</ul>
	@endif
	@endforeach
@endif
@endif

	<div class="page-heading">Welcome Admin, These are the users</div>
	<table class="table table-hover table-bordered" style="font-family: 'Slabo 27px', serif;">
    	<thead>
			<tr> 
				<th>User.Id</th>
				<th>Name</th>
				<th>Email-Id</th>
				<th>Post Count</th>
				<th>Action</th>  
			</tr>
	  	</thead>
		<tbody>
		@foreach($diary as $d)
			<tr>
				<td>{{ $d->id }}</td>
				<td>{{ $d->username }}</td>
				<td>{{ $d->email }}</td>
				<td>{!! $d->count !!}</td>
				<td>
				<span><a href="viewfulluserpost/{{$d->id}}">View</a> </span>
				@if(! $d->admin_status )
				<span><a href="makeuseradmin/{{ $d->id }}">Make Admin</a></span>
				@endif
				<span id="click" value="{{ $d->id }}" onmouseover="del=false;" onmouseout="del=true;" onclick="deleteuser('{{ $d->username }}')"><a href="#">Delete</a></span>
				</td>   
			</tr>
		@endforeach
		</tbody>
	</table>

<!--<div class="container" style="margin-right:150px; text-align:right;">
        <span class="dropdown">
        	<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort Based on
        	<span class="caret"></span>
        	</button>
        	<ul class="dropdown-menu">
             <li><a href="#" id="sortusername">User</a></li>
             <li><a href="#" id="sortpostcount">Post Count</a></li>
            </ul>
        </span>
 </div>-->

 @stop