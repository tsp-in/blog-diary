<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>@yield('title') - TSP Journal</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
<link rel="	stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" />

<link href="{{ asset('resources/views/style.css') }}" rel="stylesheet">
@yield('styles')
</head>
<body>
<nav class="navbar navbar-default navbar-static-top" id="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="home">TSP Journals</a>
    </div>
		@if(Auth::check())
	    <ul class="nav navbar-nav">
	      <li class="create"><a href="{{ url('/set') }}">Add New Page</a></li>
	      <li class="view"><a href="{{ url('/view') }}">See Your Pages</a></li>
				@if(Auth::user()->admin_status)
				<li class="users"><a href="{{ url('/list') }}">List Users</a></li>
				@endif
	    </ul>
	   	<ul class="nav navbar-nav navbar-right">                    
      	<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						{{ Auth::user()->username }} <span class="caret"></span>
					</a>
          <ul class="dropdown-menu" role="button">                              							
          	<li><a href="{{ url('/settings') }}"><i class="fa fa-cog"></i> Settings</a></li>						
						@if(Auth::user()->admin_status)
						<li><a  data-toggle="modal" href="#sign-up-modal"><i class="fa fa-sign-in"></i> Add User </a></li>
						@endif
						<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>	
          </ul>
        </li>
      </ul>
		@endif
  </div>
</nav>
<div class="container">
	@yield('content')
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>

<script src="{{ asset('app.js') }}"></script>	
<script src="{{ asset('validate.js') }}"></script>	
<script src="{{ asset('bootbox.min.js') }}"></script>	
<script>		
		function deletefulluserpost(id) {
			bootbox.confirm("Are you sure you want to delete ?<br><br><span id='bootbox'> Press <strong>Ok</strong> to Delete and <strong>Cancel</strong> to revoke</span>", function(result) {
				if(result) {
					window.location = "../delete/"+id;
				}
			}); 
		}
		
		function deletepost(id) {
			bootbox.confirm("Are you sure you want to delete ?<br><br><span id='bootbox'> Press <strong>Ok</strong> to Delete and <strong>Cancel</strong> to revoke</span>", function(result) {
				if(result) {
					window.location = "delete/"+id;
				}
			});
		}
		
		function deleteuser(id) {
			if(id !== "undefined"){
				bootbox.confirm("Are you sure you want to delete user?<br><br><span id='bootbox'> Press <strong>Ok</strong> to Delete and <strong>Cancel</strong> to revoke</span>", function(result) {
					if(result) {
						window.location = "delete/"+id;
					}
				}); 
			}
		}
</script>

@yield('scripts')

<div class="modal" id="sign-up-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" > <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign Up</h4>
      </div>
	  
	{!! Form::open(array('url' => '/adduser','id' => 'adduser')) !!}
	  
      <div class="modal-body">
	  <fieldset>
	  <div class="form-group col-lg-6 col-lg-offset-3">
					<input type="text" name="name" id="name" placeholder="name" value="{{ old('name') }}"  class="form-control" required="required" pattern="[A-Za-z0-9 ]+{3,60}" title="Min.three and max.60 alphanumeric characters are only allowed">
						</input>
        </div>
	  
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="text" name="username" id="username" placeholder="username" value="{{ old('username') }}"  class="form-control" required="required" pattern="[A-Za-z0-9 ]+{3,60}" title="Min.three and max.60 alphanumeric characters are only allowed">
	  </input>			
	  </div>
	  
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="email" name="email" id="email" placeholder="email" value="{{ old('email') }}" class="form-control" required="required">
	  </input>
	  </div>
	  
	  <div class="form-group col-lg-6 col-lg-offset-3">
	  <input type="password" name="password" id="password" placeholder="password" class="form-control" autocomplete="off" required="required" pattern="[A-Za-z0-9]{3,60}" title="Min.three and max.60 alphanumeric characters are only allowed">
	  </input>
	  </div>
	  </fieldset>
	  </div>
      <div class="modal-footer">
        <input type="submit" id="signUpSubmit" class="btn btn-primary" value="submit"></input>
      </div>
	 
    </div>
	
	{!! Form::close() !!}
	
	</div>
  </div>
</body>
</html>