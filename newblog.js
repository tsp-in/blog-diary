$(document).ready(function() {
	function validateEditor() {
		$("#newblog").bootstrapValidator('revalidateField','learning');
	};
    $('#newblog').bootstrapValidator({
            framework: 'bootstrap',
			excluded: [':disabled'],	
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
            fields: {
				date: {
                    validators: {											
						date: {
							format: 'MM/DD/YYYY',
							message: 'The value is not a valid date'
						},
						notEmpty: {
                            message: 'The date is required'
                        }
                    }
				},
                task: {
                    validators: {
                        notEmpty: {
                            message: 'The task is required and cannot be empty'
                        }
                    }
				},
				learning: {
                    validators: {
                        notEmpty: {
                            message: 'The learning is required and cannot be empty'
                        }
                    }
				}              
            }
        })
		.on('summernote.change', function(customEvent, contents, $editable) {
                // Revalidate the content when its value is changed by Summernote
                $('#newblog').bootstrapValidator('revalidateField', 'learning');
				$('#newblog').bootstrapValidator('revalidateField', 'task');
            })
		 .find('[id="date"]')
            .datepicker({			
                onSelect: function(date, inst) {                   		
                    $('#newblog').bootstrapValidator('revalidateField', 'date');
                }
            })
            .end();
});

