<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDiaryAndRemoveTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diaries', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('content');
            $table->text('task');
            $table->text('learning');
            $table->text('problems');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diaries', function (Blueprint $table) {
            $table->dropColumn('task');
            $table->dropColumn('learning');
            $table->dropColumn('problems');
            $table->string('title');
            $table->text('content');
        });
    }
}
