<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Diary extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "diaries";
     
    protected $fillable = [
        'date', 'task', 'learning','problems','username','writer_id',
    ];


    public function getback()
    {
        return $this->belongsTo("App\User", "writer_id");
    }
}
