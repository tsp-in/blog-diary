<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
	
	Route::get('/', function () {
	    return redirect('/home');
	});

	Route::get('diary','AuthController@indexDiary');
	Route::post('adduser','AuthController@addUser');
	
	Route::get('home', 'DiaryController@homeDiary');

	Route::post('signup','AuthController@signUp');
	Route::post('signin','AuthController@signIn');
	Route::get('logout', 'AuthController@logOut');
	Route::get('set',array('as'=>'set','uses'=>'DiaryController@setdiary'));
	Route::get('view',array('as'=>'view','uses'=>'DiaryController@viewDiary'));
	Route::post('view',array('as'=>'view','before'=>'auth','uses'=>'DiaryController@postDiary'));
	Route::get('viewfulluserpost/{id}','DiaryController@viewFullUserPost');
	
	Route::get('read/{id}','DiaryController@readDiary');
	Route::get('edit/{id}','DiaryController@editDiary');
	Route::get('delete/{id}','DiaryController@deleteDiary');
	Route::post('edit', 'DiaryController@editPost');
	Route::get('sort/{id}','DiaryController@sortDiary');
	Route::get('list','DiaryController@listUser');
	Route::get('makeuseradmin/{id}','DiaryController@makeAdmin');
	Route::get('settings','AuthController@getSettings');
	Route::post('changename','AuthController@changeName');
	Route::post('changepassword','AuthController@changePassword');
	
});