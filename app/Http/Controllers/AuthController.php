<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Input;
use App\User;
use Hash;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {
	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/view';

    public function __construct() {
        $this->middleware('guest', ['except' => ['logOut','getSettings','changeName','changePassword','addUser']]);
    }
	
	public function indexDiary() {
		if(Auth::check()) {
			return view("view");
		}
		else {
			return view("diary");
		}
	}
	
	public function signIn() {
		$signin = array(
			'username' => 'required|max:60',
			'password' => 'required|max:60',
		);

	$validation = Validator::make(Input::all(),$signin);
	
	if($validation->fails()){
			$errors = $validation->errors();
			return \Redirect::to('/diary')->withErrors($errors);
	    }
		
		if(Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password')),$remember = true)){
			return \Redirect::to('/view');
		}
		
		$errors = "No such user exists";
		return \Redirect::to('/diary')->withErrors($errors);
	}
	
	public function logOut() {
		Auth::logout();
		return redirect('/diary');
    }
	
	public function getSettings() {
		if(Auth::check())
			return view("settings")->with("name",Auth::user()->name);
		else {
			return \Redirect::to("diary");
		}
	}
	
	public function changeName() {
		if(Auth::check()) {
		$name = Input::get("name");
		$oldname = User::where("username",Auth::user()->username)->pluck("name");
		
		$check = array(
			'name' => 'required|max:60|unique:users',
		);

	$validation = Validator::make(Input::all(),$check);
	
	if($validation->fails()){
			$errors = $validation->errors();
			return \Redirect::to('/settings')->withErrors($errors);
	    }
		
		User::where("username",Auth::user()->username)->update(["name" => $name]);
		return \Redirect::back()->withErrors(['msg','Updated Successfully']);
		}
		else {
			return \Redirect::to("diary");
		}
	}
	
	public function changePassword() {
		if(Auth::check()) {
		$pwd = Hash::make(Input::get("pwd"));
		$credentials = [
		"id"  => Auth::user()->id
		];
		$pass = User::where($credentials)->pluck("password");
		$hash = Input::get("oldpwd");
		if(Hash::check($hash,$pass[0])) {
			User::where($credentials)->update(["password" => $pwd]);
			return \Redirect::back()->withErrors(['msg','Updated Successfully']);
		}
		return \Redirect::back()->withErrors(['msg','Old Password didn\'t match']);
		}
		else {
			return \Redirect::to("diary");
		}
	}
	
	public function addUser(Request $request) {
		if(Auth::check() && Auth::user()->admin_status) {
			$signup = array(
			'name' => 'required',
			'username' => 'unique:users',
			'email' => 'unique:users',
		);
	$validation = Validator::make($request->all(),$signup);
	$str = \URL::previous();
	$string = "";
	for($i=strlen($str)-1;$i>=0;$i--)
		if($str[$i]!="/")
			$string = $string.$str[$i];
		else 
			break;
	$geturl = strrev($string);
	if($validation->fails()) {
		$request->flashOnly('username', 'email');
		$errors = $validation->errors();
		return \Redirect::to($geturl)->withErrors($errors);
	}
	else{
		User::create([
			'name' => Input::get('name'),
			'username' => Input::get('username'),
			'password' => Hash::make(Input::get('password')),
			'email' => Input::get('email'),
			'updated_at' => Carbon::now(),
			'created_at' => Carbon::now()		
		]);	
		$errors = "User Added !!!";
		return \Redirect::back()->withErrors(["errors",$errors]);
	}
	}
	else {
			return \Redirect::back();
		}
}
}