<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Validator;
use Input;
use App\User;
use App\Diary;
use Hash;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class DiaryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function homeDiary()
    {
        if (Auth::user()->admin_status) {
            $diary = Diary::join('users', 'users.id', '=', 'diaries.writer_id')
                        ->select(['diaries.*', 'users.id AS user_id', 'users.username AS username'])
                        ->orderBy('updated_at', 'asc')
                        ->limit(10)->get();
            return view("adminhome", compact("diary"));
        } else {
            $diary = Diary::join('users', 'users.id', '=', 'diaries.writer_id')
                        ->select(['diaries.*', 'users.id AS user_id', 'users.username AS username'])
                        ->where('diaries.writer_id', Auth::user()->id)
                        ->limit(10)->get();
            return view("home", compact(['diary']));
        }
    }
    
    public function setDiary()
    {
        
        return view("set");
    }
    
    public function viewDiary()
    {
        if (Auth::user()->admin_status) {
            $diary = Diary::select(['diaries.*'])->with('getBack')->get();
            return view("view", compact(["diary"]))->with("msg", "");
        } else {
            $diary = Diary::with('getBack')->select(['diaries.*'])->where('diaries.writer_id', Auth::user()->id)->get();
            return view("view", compact(["diary"]))->with("msg", "");
        }
    }
    
    public function postDiary(Request $request)
    {
        $task = $temptask = Input::get("task");
        $learning = $templearning = Input::get("learning");
        
        $task = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $task);
        $task = preg_replace('/\s+/', '', $task);
        $task = strip_tags($task);
        
        $learning = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $learning);
        $learning = preg_replace('/\s+/', '', $learning);
        $learning = strip_tags($learning);
        
        $request->request->add(['task' => $task,'learning' => $learning]);
        $check = array(
            'task' => 'required|min:6',
            'learning' => 'required|min:15'
        );
        $validation = Validator::make($request->all(), $check);
        
        if ($validation->fails()) {
            $request->flashOnly('learning', 'task');
            return \Redirect::back()->withErrors(['msg','Task and learning fields should be meaningful']);
        }
        
        $request->request->add(['task' => $temptask,'learning' => $templearning]);
        
        $new = new Diary();
        $new->task = Input::get('task');
        $new->learning = Input::get('learning');
        $new->problems = Input::get('problems');
        $new->date = Input::get('date');
        $new->writer_id = Auth::user()->id;
        $new->save();
        $diary = Diary::where('writer_id', Auth::user()->id)->get();
        return \Redirect::to("view")->withErrors(['msg','Set Succesfully']);
    }

    public function readDiary($id)
    {
        $query = Diary::with('getBack')->where('id', $id);
        if (!Auth::user()->admin_status) {
            $query->where('writer_id', Auth::user()->id);
        }
        $read = $query->first();
        if ($read) {
            return view("read", compact('read'));
        } else {
            return redirect('/view')->withErrors(['msg', 'Either diary does not exists or you are not allowed to view it.']);
        }
    }
    
    
    public function editDiary($id)
    {
        if (!Auth::user()->admin_status) {
            $check = array(
            'writer_id' => Auth::user()->id,
            'id' => $id
            );
            $auth = Diary::where($check)->get();
            if ($auth == "[]") {
                return \Redirect::back()->withErrors(['msg','Well !! You are not allowed to view other\'s diary.. Try Yours ..']);
            }
            $edit = Diary::find($id);
        } else {
            $edit = Diary::find($id);
        }
        return view("edit", compact("edit"));
    }
  
    public function deleteDiary($id)
    {
        if (Auth::user()->admin_status) {
            $deleteuser = User::where('username', $id)->get();
            $deletepost = Diary::find($id);
            if ($deleteuser == "[]") {
                $deletepost->delete();
                return \Redirect::back()->withErrors(['msg','Diary Deleted Succesfully']);
            } else {
                $deleteuser[0]->delete();
                return \Redirect::back()->withErrors(['msg','User Deleted Succesfully']);
            }
        } else {
            $del = Diary::find($id);
            $del->delete();
            return \Redirect::back()->withErrors(['msg','User Deleted Succesfully']);
        }
    }
  
    public function editPost(Request $request)
    {
        $id = Input::get("edit");
        $task = $temptask = Input::get("task");
        $learning = $templearning = Input::get("learning");
        
        $task = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $task);
        $task = preg_replace('/\s+/', '', $task);
        $task = strip_tags($task);
        
        $learning = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $learning);
        $learning = preg_replace('/\s+/', '', $learning);
        $learning = strip_tags($learning);
        
        $request->request->add(['task' => $task,'learning' => $learning]);
        $id = Input::get("editcheck");
        $check = array(
            'task' => 'required|min:6',
            'learning' => 'required|min:15',
            'date' => 'required|date'
        );
        $validation = Validator::make($request->all(), $check);
        
        if ($validation->fails()) {
            $request->flashOnly('learning', 'task', 'date');
            return \Redirect::back()->withErrors(['msg','date and Task (more than 6 characters) and learning (more than 15 characters) fields should be meaningful']);
        }
        if ($id) {
            $request->request->add(['task' => $temptask,'learning' => $templearning]);
            Diary::where('id', $id)->update(array('date' => Input::get("date"),'task' => Input::get('task'),'learning' => Input::get('learning'), 'problems' => Input::get('problems')));
            $diary = Diary::where('id', $id)->get();
            $msg = "Updated Succesfully";
            return \Redirect::to("view")->withErrors($msg);
        } else {
            return \Redirect::back()->withErrors(['msg', 'Do not try change Post Id']);
        }
    }
  
    public function sortDiary($str)
    {
        $f = true;
/*		if( strpos(\URL::previous(),'list') !== false && Auth::user()->admin_status){
			$page = "list";
		}
		else {
			$page = "view";
		}*/
        
        if ($str == "username" && !Auth::user()->admin_status) {
            $f = false;
            $diary = Diary::join('users', 'users.id', '=', 'diaries.writer_id')
            ->select(['diaries.*', 'users.id AS user_id', 'users.username AS username'])
            ->orderBy('users.username')->get();
        } elseif ($str == "username" && Auth::user()->admin_status) {
            $f = false;
            $diary = \DB::select(\DB::raw('select( select count(writer_id) from diaries where diaries.writer_id=users.id) as count,id as id,username as username,email as email,admin_status as admin_status from users order by username;'));
        } elseif ($str == "datetime" && !Auth::user()->admin_status) {
            $diary = Diary::join('users', 'users.id', '=', 'diaries.writer_id')->select(['diaries.*','users.username'])->where("diaries.writer_id", Auth::user()->id)->orderBy('updated_at', 'asc')->get();
        } elseif ($str == "datetime" && Auth::user()->admin_status) {
            $diary = Diary::join('users', 'users.id', '=', 'diaries.writer_id')->select(['diaries.*','users.username'])->orderBy('date', 'asc')->get();
        } elseif ($str == "postcount" && Auth::user()->admin_status) {
            $f = false;
            $diary = \DB::select(\DB::raw('select( select count(writer_id) from diaries where diaries.writer_id=users.id) as count,id as id,username as username,email as email,admin_status as admin_status from users order by count;'));
        } elseif ($str == "task" && !Auth::user()->admin_status) {
            $diary = Diary::select(['diaries.*'])->where("diaries.writer_id", Auth::user()->id)->orderBy('task', 'asc')->get();
        } elseif ($str == "task" && Auth::user()->admin_status) {
            $diary = Diary::select(['diaries.*'])->orderBy('task', 'asc')->get();
        } else {
            ;
        }
        if ($f) {
            foreach ($diary as $d) {
                $d->updated = $d->updated_at->diffForHumans(\Carbon\Carbon::now());
                $d->username = $d->getback->username;
            }
        }
        return response()->json(['diary' => $diary]);
    }
    
    public function listUser()
    {
        $count = 0;
        if (Auth::user()->admin_status) {
            $diary = \DB::select(\DB::raw('select( select count(writer_id) from diaries where diaries.writer_id=users.id) as count,id as id,username as username,email as email,admin_status as admin_status from users;'));
        }
        return view("list", compact("diary"), ['count' => $count]);
    }
  
    public function makeAdmin($id)
    {
        if (Auth::user()->admin_status) {
            User::where('id', $id)->update(['admin_status'=>'1']);
            $diary = User::all();
            return \Redirect::back()->withErrors(['message', 'admin added']);
        }
    }
    
    public function viewFullUserPost($id)
    {
        if (Auth::user()->admin_status) {
            $diary = Diary::where("writer_id", $id)->orderBy("updated_at", "desc")->get();
            if ($diary == "[]") {
                return \Redirect::back()->withErrors(['msg',"Sorry The selected user has no diaries"]);
            }
            return view("viewfulluserpost", compact("diary"))->with("msg", "");
        }
    }
}
