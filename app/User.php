<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Diary;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
     
    protected $primaryKey = "id";
    protected $fillable = [
        'name', 'email', 'password','username','count',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function getdiary()
    {
        return $this->hasMany("App\Diary", "writer_id");
    }
}
