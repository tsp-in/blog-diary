function confirmed() {
	 var password = $("#pwd").val();
	 var confirmPassword = $("#pwd_confirmation").val();
		if (password != confirmPassword) {
			return false;
		}
		else {
			return true;
		}
}

function isPasswordMatch() {
    var password = $("#pwd").val();
    var confirmPassword = $("#pwd_confirmation").val();

    if (password != confirmPassword) {
		$("#password_notmatched").html("<strong style='color:brown'>Passwords do not match!");
	}
	else {
		$("#password_notmatched").html("<strong style='color:green'>Passwords matched!");
	}
}

$(document).ready(function () {
    $("#pwd_confirmation").keyup(isPasswordMatch);
});

