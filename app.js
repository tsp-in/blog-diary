$(document).ready(function(){ 

var del = true;

$("#changepwd").hide();
$("#changename").hide();
$("#press").click(function(){ 
	    window.history.back();
});

$("#showpwdchangeform").click(function(){ 
		$("#changepwd").show();
		$("#changename").hide();
});

$("#showusernamechangeform").click(function(){ 
	    $("#changepwd").hide();
		$("#changename").show();
});

$("#sortusername").on("click",function(event){
		event.preventDefault();
		$.ajax({
                    url: 'sort/username',
                    type: 'get',
                    dataType: 'json',                    
                    success: function(diary)
                    {																		
						for(i=0;i<diary.diary.length;i++) {						
							$('#row'+i).attr('onclick','if (del) readfulluserpost("'+diary.diary[i].id+'")');
							if(!diary.diary[i].admin_status) {
								$('#row'+i).html('<td>'+diary.diary[i].id+'</td><td>'+diary.diary[i].username+'</td><td>'+diary.diary[i].email+'</td><td>'+diary.diary[i].count+'</td><td><span><a href="read/'+diary.diary[i].username+'">Read </a></span><span> <a href="makeuseradmin/'+diary.diary[i].id+'">Make Admin </a></span><span id="click" onmouseover="del=false;" onmouseout="del=true;" value="'+diary.diary[i].username+'" onclick="deleteuser(\'' + diary.diary[i].username + '\');"><a href="#" >Delete</a></span></td>');							
							}
							else {
								$('#row'+i).html('<td>'+diary.diary[i].id+'</td><td>'+diary.diary[i].username+'</td><td>'+diary.diary[i].email+'</td><td>'+diary.diary[i].count+'</td><td><span><a href="read/'+diary.diary[i].username+'">Read </a></span><span id="click" onmouseover="del=false;" onmouseout="del=true;" value="'+diary.diary[i].id+'" onclick="deleteuser(\'' + diary.diary[i].username + '\');"><a href="#" >Delete</a></span></td>');							
							}						
						}
                                               
                    }
                })
	
});

$("#sortpostcount").on("click",function(event){
		event.preventDefault();
		$.ajax({
                    url: 'sort/postcount',
                    type: 'get',
                    dataType: 'json',                    
                    success: function(diary)
                    {																		
						for(i=0;i<diary.diary.length;i++) {						
							$('#row'+i).attr('onclick','if (del) readfulluserpost("'+diary.diary[i].id+'")');
							if(!diary.diary[i].admin_status) {
								$('#row'+i).html('<td>'+diary.diary[i].id+'</td><td>'+diary.diary[i].username+'</td><td>'+diary.diary[i].email+'</td><td>'+diary.diary[i].count+'</td><td><span><a href="read/'+diary.diary[i].username+'">Read </a></span><span> <a href="makeuseradmin/'+diary.diary[i].id+'">Make Admin </a></span><span id="click" onclick="deleteuser(\'' + diary.diary[i].username + '\');" onmouseover="del=false;" onmouseout="del=true;" value="'+diary.diary[i].username+'"><a href="#" >Delete</a></span></td>');							
							}
							else {
								$('#row'+i).html('<td>'+diary.diary[i].id+'</td><td>'+diary.diary[i].username+'</td><td>'+diary.diary[i].email+'</td><td>'+diary.diary[i].count+'</td><td><span><a href="read/'+diary.diary[i].username+'">Read </a></span><span id="click" onclick="deleteuser(\'' + diary.diary[i].username + '\');" onmouseover="del=false;" onmouseout="del=true;"><a href="#">Delete</a></span></td>');							
							}							
						}
                                               
                    }
                })
	
});

$("#sortdatetime").on("click",function(event){
	 	event.preventDefault();
		$.ajax({
                    url: 'sort/datetime',
                    type: 'get',
                    dataType: 'json',                    
                    success: function(diary)
                    {																		
						for(i=0;i<diary.diary.length;i++) {						
							$('#row'+i).html('<td>'+diary.diary[i].id+'</td><td>'+diary.diary[i].username+'</td><td>'+diary.diary[i].date+'</td><td>'+diary.diary[i].updated+'</td><td>'+diary.diary[i].task+'</td><td><span><a href="read/'+diary.diary[i].id+'">Read </a></span><span> <a href="edit/'+diary.diary[i].id+'">Edit </a></span><span id="click" value="'+diary.diary[i].id+'" onclick="deletepost('+diary.diary[i].id+')"><a href="#" >Delete</a></span></td>');							
						}
                                               
                    }
                })
});

$("#sorttask").on("click",function(event){
	   	event.preventDefault();
		$.ajax({
                    url: 'sort/task',
                    type: 'get',
                    dataType: 'json',                    
                    success: function(diary)
                    {						
						for(i=0;i<diary.diary.length;i++) {											
							$('#row'+i).html('<td>'+diary.diary[i].id+'</td><td>'+diary.diary[i].username+'</td><td>'+diary.diary[i].date+'</td><td>'+diary.diary[i].updated+'</td><td>'+diary.diary[i].task+'</td><td><span><a href="read/'+diary.diary[i].id+'">Read </a></span><span> <a href="edit/'+diary.diary[i].id+'">Edit </a></span><span id="click" value="'+diary.diary[i].id+'" onclick="deletepost('+diary.diary[i].id+')"><a href="#" >Delete</a></span></td>');							
						}
                                               
                    }
                })
});


});